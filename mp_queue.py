#!/usr/bin/env python3
import multiprocessing as mp
import time
import queue as Queue

nums = range(1000000)


class IQ(object):
	''' Another option for an iterable queue '''
	def __init__(self, maxsize=0):
        	self.q = mp.Queue(maxsize)

	def __getattr__(self, attr):
		if hasattr(self.q, attr):
            		return getattr(self.q, attr)
	def __iter__(self):
		return self

	def next(self):
		item = self.q.get()
		if item == 'None':
			raise StopIteration
		return item

def f(x):
	''' Adding items to the queue '''
	f.q.put(x)
      
def f_init(q):
	''' Initializer list magic to be able to multiprocess queue object'''
	f.q = q


def some_func(x):
	return x*x+3*x+4

if __name__=='__main__':
	start = time.time()
	q = mp.Queue()
	p = mp.Pool(None, f_init, (q,))
	r = p.map_async(f,nums)
	r.wait()
	''' Put a sentinel to stop iteration '''
	q.put(None)
	"""
	while not q.empty():
		print(q.get())
	p.close()
	"""
	''' map is much faster than iterable map(imap) '''
	result = p.map(some_func,iter(q.get,None))
	for val in list(result)[-5:-1]:
		print(val)
	print("Total time taken is ", (time.time()-start))


	start = time.time()
	q2 = Queue.Queue()

	for i in nums:
		q2.put(i)
#	while not q2.empty():
#               print(q2.get())
	print("Total time taken is ", (time.time()-start))

