# Sample 4 #

A nifty way to add elements to a shared async queue and then compute the action of a function on those variables in SHM parallel environment. Multiprocessing circumvents GIL by launching multiple process. Adding elements to the queue in parallel might see decent speed-up but the real benefit is in computing the action in parallel. Right now the queue elements and the action (function) on the elements are completely useless, however, the possibilities are obvious :-).

### Dependencies ###

* Python 3
* Multiprocessing
* Queue